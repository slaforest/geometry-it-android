package com.zootowntech.geometryit;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

public class MainActivity extends FragmentActivity implements
        ActionBar.TabListener{
    private boolean mTwoPane;
    ActionBar actionBar;

    //Globals
    private long lastFormulaGet;
    private JSONArray mFormulas;
    private long lastGlossaryGet;
    private JSONArray mTerms;

    //Getters and setters
    public void setLastFormulaGet(long time){
        this.lastFormulaGet = time;
    }

    public long getLastFormulaGet(){
        return this.lastFormulaGet;
    }

    public void setFormulas(JSONArray jsonArray){
        this.mFormulas = jsonArray;
    }

    public JSONArray getFormulas(){
        return this.mFormulas;
    }
    public void setLastGlossaryGet(long time){
        this.lastGlossaryGet = time;
    }

    public long getLastGlossaryGet(){
        return this.lastGlossaryGet;
    }

    public void setTerms(JSONArray jsonArray){
        this.mTerms = jsonArray;
    }

    public JSONArray getTerms(){
        return this.mTerms;
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tabState", actionBar.getSelectedTab().getPosition());


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_main);
        /*if ( customTitleSupported ) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
        }
        final TextView myTitleText = (TextView) findViewById(R.id.myTitle);
        if ( myTitleText != null ) {
            myTitleText.setText("NEW TITLE");

            // user can also set color using "Color" and then "Color value constant"
            // myTitleText.setBackgroundColor(Color.GREEN);
        }*/

        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.addTab(actionBar.newTab().setText(R.string.title_activity_tools_list_fragment)
                .setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.title_activity_glossary_fragment)
                .setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.title_activity_formulas_fragment)
                .setTabListener(this));
        if( savedInstanceState != null ){
           // Toast.makeText(this, "pos " + savedInstanceState.getInt("tagState"), Toast.LENGTH_LONG).show();
           actionBar.setSelectedNavigationItem (savedInstanceState.getInt("tabState"));
        }


        // For each of the sections in the app, add a tab to the action bar.



        if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE || (getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            // on a large screen device ...
            mTwoPane = true;
            Fragment newFragment = null;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        int currentTab = actionBar.getSelectedNavigationIndex();
        //String text = currentTab.getText().toString();
           // Toast.makeText(this, text, Toast.LENGTH_LONG);
       // Log.d("current tab", "ey " + text);
        if (currentTab == 0){
            newFragment = new ToolsListFragment();

            ((ToolsListFragment) newFragment).setDisplayType(true);
            transaction.replace(R.id.container, newFragment);
            transaction.commit();
        }else if (currentTab == 1){
            newFragment = new GlossaryFragment();
            ((GlossaryFragment) newFragment).setDisplayType(true);
            //}
            transaction.replace(R.id.container, newFragment);
            transaction.commit();
        }else{
            newFragment = new FormulasFragment();
            ((FormulasFragment) newFragment).setDisplayType(true);
            //}
            transaction.replace(R.id.container, newFragment);
            transaction.commit();
        }


    }}
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {

        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
       /*Fragment newFragment = null;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        int currentTab = actionBar.getSelectedNavigationIndex();
        Log.d("current tab", "ey " + currentTab);
        if (currentTab == 0){
            newFragment = new ToolsListFragment();

            ((ToolsListFragment) newFragment).setDisplayType(true);
        }else{
            newFragment = new GlossaryFragment();
            ((GlossaryFragment) newFragment).setDisplayType(true);
        //}
        transaction.replace(R.id.container, newFragment);
        transaction.commit();*/

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
        // TODO Auto-generated method stub


    }

    @Override
    public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
        // TODO Auto-generated method stub
        int pos = tab.getPosition();
        Fragment newFrag =null;
        //Toast.makeText(this, "pos" +pos, Toast.LENGTH_LONG).show();
        if (pos == 0){
            newFrag = new ToolsListFragment();
            ((ToolsListFragment) newFrag).setDisplayType(mTwoPane);

        }else if(pos==1){
            newFrag = new GlossaryFragment();
            ((GlossaryFragment) newFrag).setDisplayType(mTwoPane);

        }else{
            newFrag = new FormulasFragment();
            ((FormulasFragment) newFrag).setDisplayType(mTwoPane);

        }
        if (mTwoPane){
            DetailFragment detFrag = new DetailFragment();
            detFrag.setTitleLabel("");
            detFrag.setMyHtmlString("");
            getSupportFragmentManager().beginTransaction().replace(R.id.detail_container, detFrag).commit();
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, newFrag).commit();
    }

    @Override
    public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
        // TODO Auto-generated method stub

    }


}