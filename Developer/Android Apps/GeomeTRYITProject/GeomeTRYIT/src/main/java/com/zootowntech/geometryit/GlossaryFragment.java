package com.zootowntech.geometryit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Picture;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextWatcher;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.EditText;
import android.text.Editable;
import android.text.TextWatcher;

public class GlossaryFragment extends ListFragment {
    JSONArray mterms;
    ArrayList<String> mtermsList;
    ArrayAdapter<String> adapter;
    ArrayList< ArrayList<String>> organizedTerms;
    SeparatedListAdapter sla;
    boolean mTwoPane;
    EditText inputSearch;
    TextView titleLabel;

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void setDisplayType(boolean isTwoPane){
        mTwoPane = isTwoPane;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //getActivity().setContentView(R.layout.activity_main);
        setRetainInstance(true);
        Activity act = getActivity();
        long lastGet = 0;
        if(act instanceof MainActivity) {
            lastGet = ((MainActivity) act).getLastGlossaryGet();
        }
        Calendar now = Calendar.getInstance();
        long currTime = now.getTimeInMillis();
        long diff = currTime - lastGet;
        //Log.e("times", "curr - last = " + diff);
        if ((diff > 86400000) || (diff == currTime) ){
            if(isOnline()){
                new LocalAsyncTask().execute("http://www.geometry-it.appspot.com/glossary");
            }else{
                didNotGetFile();
            }

        }else{

            this.mterms = ((MainActivity) act).getTerms();
            setMterms(mterms);

        }

    }

    public void updateList(ArrayList<String> termsList){

       organizedTerms= new ArrayList<ArrayList<String>>();
        ArrayList<String> letterList= new ArrayList<String>();
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.my_list_view, R.id.list_content, termsList);
        sla = new SeparatedListAdapter(getActivity());
        //ArrayAdapter<String> currAdapter =
        for(int i = 0; i < mterms.length(); i++){
            //Log.d("test", termsArray.toString());
            String currStr = null;
            try{
                JSONObject j = mterms.getJSONObject(i);
                currStr = j.getString("term");

            }catch (Exception e){
                //oops
            }
            String firstLetter = currStr.substring(0,1);
            //Log.d("test", currStr);
            // Log.d("test", firstLetter);
            if (letterList.size() == 0) {
                letterList.add(firstLetter);
            }
            else if(!letterList.contains(firstLetter)){
                    letterList.add(firstLetter);
            }

        }

        //NSMutableArray* mutArray = [[NSMutableArray alloc]init];
        for (int i = 0 ; i < letterList.size(); i++) {
            String currentLetter = letterList.get(i);
            ArrayList<String> currentTermsInSection = new ArrayList<String>();
            for (int k = 0; k < termsList.size(); k++){

                String firstLetter =  termsList.get(k).substring(0,1);
                if (firstLetter.equals(currentLetter)) {
                    currentTermsInSection.add(termsList.get(k));

                }
            }

            organizedTerms.add(currentTermsInSection);
        }

        for (int k = 0;k<organizedTerms.size();k++){
            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item, organizedTerms.get(k));
            sla.addSection(letterList.get(k), listAdapter);
        }

        setListAdapter(sla);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_glossary, container, false);
        if (v != null){
        inputSearch = (EditText) v.findViewById(R.id.input_search);
        }
        if (isOnline()){
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                boolean isFound= false;
                String searchString=inputSearch.getText().toString();
                int textLength=searchString.length();
               // Log.e("searchString ", searchString);

                //clear the initial data set

                adapter.clear();

                for(int i=0; i<mterms.length();i++)
                {
                    String termName="error";
                    try {
                        JSONObject jsonObject = mterms.getJSONObject(i);
                        termName =jsonObject.getString("term");
                    } catch (Exception e) {
                       // Log.d("GetTermsJSON", e.getLocalizedMessage());
                        Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

                    }
                   // Log.e("termname ", termName);

                    if(textLength<=termName.length() && textLength > 0){
                        //compare the String in EditText with Names in the ArrayList
                        if(searchString.equalsIgnoreCase(termName.substring(0,textLength))){
                            adapter.add(termName);
                            isFound = true;
                        }
                    }

                }
                if(isFound && textLength != 0){
                    setListAdapter(adapter);
                }else{
                    setMterms(mterms);
                    setListAdapter(sla);
                }


            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        }
        return v;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        JSONObject obj = null;
        String def = null;
        String term = null;
        int index = 0;
        ArrayList<String> termsList;
        TextView tview;


        if (v.getClass().toString().equals("class android.widget.TextView")){
            tview = ((TextView)v);
            term = tview.getText().toString();
            //setMterms(mterms);

            //tview.setTextColor(Color.MAGENTA);
            index = mtermsList.indexOf(term);
        }else{
            Log.e("v's class ", v.getClass().toString());
            tview = ((TextView)v.findViewById(R.id.list_content));
            term = tview.getText().toString();
            /*for (int i = 0; i < l.getCount(); i++){
                View tempv = (View)l.getItemAtPosition(i);
                TextView tv = (TextView)tempv.findViewById(R.id.list_content);
                tv.setTextColor(Color.BLACK);
            }*/
            //tview.setTextColor(Color.MAGENTA);
            termsList = getTermsList(mterms);
            index = termsList.indexOf(term);
        }




        //Log.e("term index", index + "");
        try {
            obj = mterms.getJSONObject(index);
            def = obj.getString("definition");
            //term = obj.getString("term");
        } catch (Exception e) {
            //Log.d("GetTermsJSON", e.getLocalizedMessage());
        }


        Fragment newFragment = new DetailFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        ((DetailFragment) newFragment).setMyHtmlString(def);
        ((DetailFragment) newFragment).setTitleLabel(term);

        if (mTwoPane){
            //Log.d("GetTermsJSON", term + " " + def);
            transaction.replace(R.id.detail_container, newFragment);

        }
        else{

            transaction.replace(R.id.container, newFragment);

        }
        transaction.addToBackStack("Tools");
        transaction.commit();
    }

    public class LocalAsyncTask extends AsyncTask<String, Integer, String>{

        public String readJSONFeed(String urls) {
            StringBuilder stringBuilder = new StringBuilder();
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(urls);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    inputStream.close();
                } else {
                    //Log.d("JSON", "Failed to download file");

                }
            } catch (Exception e) {
                //Log.d("readJSONFeed", e.getLocalizedMessage());
            }
            return stringBuilder.toString();
        }

        @Override
        protected String doInBackground(String... urls) {

            return readJSONFeed(urls[0]);

        }
        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {

            try {
                JSONArray innerJsonArray = new JSONArray(result);
                JSONObject jsonObject = innerJsonArray.getJSONObject(0);

                Calendar now = Calendar.getInstance();
                long lastJSONGet = now.getTimeInMillis();
                Activity act = getActivity();
                if(act instanceof MainActivity) {
                    ((MainActivity) act).setLastGlossaryGet(lastJSONGet);
                }
               // Log.e("requested JSON", "again" + lastJSONGet);
                setMterms(innerJsonArray);

            } catch (Exception e) {
                //setMterms(null);
                //Log.d("GetTermsJSON", e.getLocalizedMessage());
                //Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

            }

        }
    }

    public void setMterms(JSONArray termsArray) {
        ArrayList<String> termsList= new ArrayList<String>();

        this.mterms = termsArray;

        Activity act = getActivity();
        if(act instanceof MainActivity) {
            ((MainActivity) act).setTerms(mterms);
        }
        for (int i = 0; i< mterms.length(); i++){
            try {
                JSONObject jsonObject = mterms.getJSONObject(i);
                termsList.add(jsonObject.getString("term"));
            } catch (Exception e) {

                //Log.d("bad json", e.getLocalizedMessage());
               // Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();
                break;
            }
            //Log.d("termsList gotten", termsList.toString());
        }
        if (termsList.size() == 0){
            //Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

        }
        Log.e("tag", termsList.toString());
        this.mtermsList = termsList;
        updateList(termsList);

    }

    public ArrayList<String> getTermsList(JSONArray mterms) {
        this.mterms = mterms;
        ArrayList<String> termsList= new ArrayList<String>();
        for (int i = 0; i< mterms.length(); i++){
            try {
                JSONObject jsonObject = mterms.getJSONObject(i);
                termsList.add(jsonObject.getString("term"));
            } catch (Exception e) {
                //Log.d("GetTermsJSON", e.getLocalizedMessage());
                Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

            }
            //Log.d("termsList gotten", termsList.toString());
        }
        this.mtermsList = termsList;
        return termsList;
    }

    public void didNotGetFile(){
        Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

    }
}
