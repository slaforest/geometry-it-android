package com.zootowntech.geometryit;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewFragment;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
//import com.google.ads.*;

public class DetailFragment extends Fragment {
    private String url = null;
    private  String myHtmlString = null;
    private String titleString ;
    //private AdView adView;

    public void setMyHtmlString(String myHtmlString) {
        this.myHtmlString = myHtmlString;
    }
    public void setTitleLabel(String title){
        this.titleString = title;
    }
    public void setUrl(String myUrl){
        this.url = myUrl;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LinearLayout mainView = (LinearLayout) inflater.inflate(R.layout.fragment_detail , container,false);
        // Create the adView
        //adView = new AdView(this, AdSize.BANNER, "a151affd606f3f7");

        // Lookup your LinearLayout assuming it's been given
        // the attribute android:id="@+id/mainLayout"
//        LinearLayout layout = (LinearLayout)findViewById(R.id.mainLayout);

        // Add the adView to it
        //mainView.addView(adView);

        // Initiate a generic request to load it with an ad
        //adView.loadAd(new AdRequest());
        WebView webView = (WebView)mainView.findViewById(R.id.webView1);
        TextView textView = (TextView)mainView.findViewById(R.id.titleLabel);
        textView.setText(titleString);
        webView.setWebChromeClient(new WebChromeClient());
        //webView.addJavascriptInterface(new WebAppInterface(getActivity()), "Android");
        WebSettings wbset=webView.getSettings();
        webView.requestFocus();
        wbset.setBuiltInZoomControls(false);
        wbset.setSupportZoom(true);
        wbset.setJavaScriptCanOpenWindowsAutomatically(true);
        wbset.setAllowFileAccess(true);
        wbset.setDomStorageEnabled(true);
        wbset.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading (WebView view, String url){
                //Log.e("MyUrl",url);
                view.loadUrl(url);
                return true;

            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
                Toast.makeText(getActivity(), "Oh no, " + description + "Make sure you have access to the internet.",	 Toast.LENGTH_LONG).show();
            }});



        if(url != null){
            webView.loadUrl(url);
            textView.setVisibility(View.GONE);
        }else{
            webView.loadData(myHtmlString, "text/html; charset=UTF-8", null);
        }

        return mainView;
    }

}