package com.zootowntech.geometryit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FormulasFragment extends ListFragment {
    JSONArray mterms ;
    ArrayList<String> mtermsList;
    ArrayAdapter<String> adapter;
    ArrayList< ArrayList<String>> organizedTerms;
    SeparatedListAdapter sla;
    boolean mTwoPane;
    EditText inputSearch;
    TextView titleLabel;

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    public void setDisplayType(boolean isTwoPane){
        mTwoPane = isTwoPane;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setRetainInstance(true);
        Activity act = getActivity();
        long lastGet = 0;
        if(act instanceof MainActivity) {
            lastGet = ((MainActivity) act).getLastFormulaGet();
        }
        Calendar now = Calendar.getInstance();
        long currTime = now.getTimeInMillis();
        long diff = currTime - lastGet;
        Log.e("times", "curr - last = " + lastGet);

        //ArrayList testTermsList = getTermsList(mterms);
        if ((diff > 86400000) || (diff == currTime) ){
            if(isOnline()){
                new LocalAsyncTask().execute("http://www.geometry-it.appspot.com/formulas");
            }else{
                didNotGetFile();
            }

        }else{

                this.mterms = ((MainActivity) act).getFormulas();
                setMterms(mterms);

        }



        //getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void updateList(ArrayList<String> termsList){

        organizedTerms= new ArrayList<ArrayList<String>>();
        ArrayList<String> letterList= new ArrayList<String>();
        ArrayList<String> tagList= new ArrayList<String>();
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.my_list_view, R.id.list_content, termsList);
        sla = new SeparatedListAdapter(getActivity());
        //ArrayAdapter<String> currAdapter =
        for(int i = 0; i < mterms.length(); i++){
            //Log.d("test", termsArray.toString());
            String currStr = null;
            String currTag = null;
            try{
                JSONObject j = mterms.getJSONObject(i);
                currStr = j.getString("name");
                currTag = j.getString("tag");
            }catch (Exception e){
                //oops
            }
            if (tagList.isEmpty()){
                tagList.add(currTag);
            }
            if (!tagList.contains(currTag)){
                tagList.add(currTag);
            }
           /* String firstLetter = currStr.substring(0,1);
            //Log.d("test", currStr);
            // Log.d("test", firstLetter);
            if (letterList.size() == 0) {
                letterList.add(firstLetter);
            }
            else if(!letterList.contains(firstLetter)){
                letterList.add(firstLetter);
            }
**/
        }

        //NSMutableArray* mutArray = [[NSMutableArray alloc]init];
        ArrayList<String> currTerms= termsList;
        for (int i = 0 ; i < tagList.size(); i++) {
            String currentTag = tagList.get(i);
            String myTag = null;
            ArrayList<String> currentTermsInSection = new ArrayList<String>();
            for (int k = 0; k < currTerms.size(); k++){
                try{
                    JSONObject j = mterms.getJSONObject(k);
                    myTag = j.getString("tag");
                }catch (Exception e){
                    //oops
                }

                if (currentTag.equals(myTag)) {
                    currentTermsInSection.add(currTerms.get(k));

                }
            }

            organizedTerms.add(currentTermsInSection);
        }

        for (int k = 0;k<organizedTerms.size();k++){
            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item, organizedTerms.get(k));
            sla.addSection(tagList.get(k), listAdapter);
        }

        setListAdapter(sla);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_glossary, container, false);

        inputSearch = (EditText) v.findViewById(R.id.input_search);
        if (isOnline()){
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                boolean isFound= false;
                String searchString=inputSearch.getText().toString();
                int textLength=searchString.length();
                // Log.e("searchString ", searchString);

                //clear the initial data set
                adapter.clear();

                for(int i=0; i<mterms.length();i++)
                {
                    String termName=null;
                    try {
                        JSONObject jsonObject = mterms.getJSONObject(i);
                        termName =jsonObject.getString("name");
                    } catch (Exception e) {
                       // Log.d("GetTermsJSON", e.getLocalizedMessage());
                        Toast.makeText(getActivity(), "Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

                    }
                     //Log.e("termname ", termName);

                    if(textLength<=termName.length()){
                        //compare the String in EditText with Names in the ArrayList
                        if(searchString.equalsIgnoreCase(termName.substring(0,textLength))){
                            adapter.add(termName);
                            isFound = true;
                        }
                    }

                }
                if(isFound && textLength != 0){
                    setListAdapter(adapter);
                }else{
                    setMterms(mterms);
                    setListAdapter(sla);
                }


            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        }

        return v;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        JSONObject obj = null;
        String def = null;
        String term = null;
        int index = 0;
        ArrayList<String> termsList;
        TextView tview;


        if (v.getClass().toString().equals("class android.widget.TextView")){
            tview = ((TextView)v);
            term = tview.getText().toString();
            //setMterms(mterms);

            //tview.setTextColor(Color.MAGENTA);
            index = mtermsList.indexOf(term);
        }else{
            //Log.e("v's class ", v.getClass().toString());
            tview = ((TextView)v.findViewById(R.id.list_content));
            term = tview.getText().toString();
            /*for (int i = 0; i < l.getCount(); i++){
                View tempv = (View)l.getItemAtPosition(i);
                TextView tv = (TextView)tempv.findViewById(R.id.list_content);
                tv.setTextColor(Color.BLACK);
            }*/
            //tview.setTextColor(Color.MAGENTA);
            termsList = getTermsList(mterms);

            index = termsList.indexOf(term);
        }




       // Log.e("term index", index + "");
        try {
            obj = mterms.getJSONObject(index);
            def = obj.getString("formula");
            //term = obj.getString("name");
        } catch (Exception e) {
           // Log.d("GetTermsJSON", e.getLocalizedMessage());
        }


        Fragment newFragment = new DetailFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        ((DetailFragment) newFragment).setMyHtmlString(def);
        ((DetailFragment) newFragment).setTitleLabel(term);

        if (mTwoPane){
            //Log.d("GetTermsJSON", term + " " + def);
            transaction.replace(R.id.detail_container, newFragment);

        }
        else{

            transaction.replace(R.id.container, newFragment);

        }
        transaction.addToBackStack("Tools");
        transaction.commit();
    }

    public class LocalAsyncTask extends AsyncTask<String, Integer, String> {

        public String readJSONFeed(String urls) {
            StringBuilder stringBuilder = new StringBuilder();
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(urls);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream inputStream = entity.getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    inputStream.close();
                } else {

                    Log.d("JSON", "Failed to download file");

                }
            } catch (Exception e) {
                Log.d("readJSONFeed", e.getLocalizedMessage());

                //Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

            }
            return stringBuilder.toString();
        }

        @Override
        protected String doInBackground(String... urls) {

            return readJSONFeed(urls[0]);

        }
        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {

            try {
                JSONArray innerJsonArray = new JSONArray(result);
                JSONObject jsonObject = innerJsonArray.getJSONObject(0);
                Calendar now = Calendar.getInstance();
                long lastJSONGet = now.getTimeInMillis();
                Activity act = getActivity();
                if(act instanceof MainActivity) {
                    ((MainActivity) act).setLastFormulaGet(lastJSONGet);
                }
                Log.e("requested JSON", "again" + lastJSONGet);
                setMterms(innerJsonArray);

            } catch (Exception e) {
                //setMterms(null);
                //Log.d("GetTermsJSON", e.getLocalizedMessage());
                //Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

            }

        }
    }

    public void setMterms(JSONArray termsArray) {

        this.mterms = termsArray;
        ArrayList<String> termsList= new ArrayList<String>();
        Activity act = getActivity();
        if(act instanceof MainActivity) {
            ((MainActivity) act).setFormulas(mterms);
        }
        for (int i = 0; i< mterms.length(); i++){
            try {
                JSONObject jsonObject = mterms.getJSONObject(i);
                termsList.add(jsonObject.getString("name"));
            } catch (Exception e) {
                //Log.d("GetTermsJSON", e.getLocalizedMessage());
                Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();
                break;
            }
            //Log.d("termsList gotten", termsList.toString());
        }
        //Log.d("termsList gotten", termsArray.toString());
        this.mtermsList = termsList;
        updateList(termsList);

    }

    public ArrayList<String> getTermsList(JSONArray termsArray) {


        this.mterms = termsArray;

        ArrayList<String> termsList= new ArrayList<String>();
        for (int i = 0; i< mterms.length(); i++){
            try {
                JSONObject jsonObject = mterms.getJSONObject(i);
                termsList.add(jsonObject.getString("name"));
            } catch (Exception e) {
                //Log.d("GetTermsJSON", e.getLocalizedMessage());
                Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

            }
            //Log.d("termsList gotten", termsList.toString());
        }
        this.mtermsList = termsList;
        return termsList;
    }

    public void didNotGetFile(){
        Toast.makeText(getActivity(),"Could not connect to glossary database. Please make sure you have internet access.", Toast.LENGTH_LONG).show();

    }
}

