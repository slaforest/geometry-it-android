package com.zootowntech.geometryit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.View.OnTouchListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by scottyla on 6/10/13.
 */
public class ProtractorFragment extends Fragment {
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private Uri fileUri = null;
    List<Point> points = new ArrayList<Point>();
    ImageView imgView;
    TextView degreesLabel;
    TextView radiansLabel;
    boolean isConj = false;
    Button conjBtn;
    Paint paint = new Paint();
    private double radians = 0;
    private double degrees = 0;
    DrawView drawView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //mDetector =  new GestureDetector(getActivity(), new MyGestureListener());
        paint.setColor(Color.MAGENTA);


    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.main, menu);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RelativeLayout mainView = (RelativeLayout) inflater.inflate(R.layout.fragment_protractor , container,false);
        imgView = (ImageView)mainView.findViewById(R.id.imageView);
        radiansLabel = (TextView)mainView.findViewById(R.id.radiansLabel);
        degreesLabel = (TextView)mainView.findViewById(R.id.degreesLabel);
        conjBtn = (Button)mainView.findViewById(R.id.conjBtn);
        drawView = new DrawView(getActivity());
        drawView.setId(1);
        drawView.setBackgroundColor(Color.TRANSPARENT);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);

        lp.addRule(RelativeLayout.BELOW, radiansLabel.getId());
        drawView.setLayoutParams(lp);
        //drawView.layout(60,0,mainView.getWidth(),mainView.getHeight());
        mainView.addView(drawView);

        //mainView.addView(drawView);

        conjBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Log.e("tag", "button clicked");
                isConj = !isConj;
                drawView.setIsConjugate(isConj);

                drawView.invalidate();
            }
        });
        //imgView.setImageResource(R.drawable.acute_angle);

        /*mainView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mDetector.onTouchEvent(event);
            }
        });*/

        return mainView;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.camera:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                //fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
                //intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

                // start the image capture Intent
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                return true;
            //  ... other item selection handling
            case R.id.info:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

// 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.protractor_info_dialog_message)
                        .setTitle(R.string.protractor_info_dialog_title);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                    }
                });

// 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imgView.setImageBitmap(photo);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
        }

        public double getRadians(){
            return radians;
        }
        public void setRadians(double rad){
            String str = String.format("%.2f", rad);
            radiansLabel.setText("Radians: " + str);
            this.radians = rad;
        }
        public double getDegrees(){
            return degrees;
        }
        public void setDegrees(double deg){
            String str = String.format("%.2f", deg);
            degreesLabel.setText("Degrees: " + str);
            this.degrees = deg;
        }




    public class DrawView extends View {
        List<Point> points = new ArrayList<Point>();
        private GestureDetector mDetector;
        private static final String DEBUG_TAG = "Gestures";
        Paint paint = new Paint();
        private int touchCounter= 0;
        private boolean isConjugate;

        public void setIsConjugate(boolean yesOrNo){
            this.isConjugate = yesOrNo;

        }
        public DrawView(Context context) {
            super(context);
            setFocusable(true);
            setFocusableInTouchMode(true);
            mDetector =  new GestureDetector(context, new MyGestureListener());
            paint.setColor(Color.MAGENTA);

            this.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    //Log.e("tag", v.getClass().toString());
                    return mDetector.onTouchEvent(event);
                }
            });

            paint.setAntiAlias(true);
        }
        class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }@Override
             public boolean onSingleTapConfirmed(MotionEvent event) {
                //Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());

                Point point = new Point();
                point.x = (int)event.getX();
                point.y = (int)event.getY();
                if (points.size() > 2){
                    points.clear();
                    //touchCounter = 0;
                }
                points.add(point);

                invalidate();
                //touchCounter++;
                return true;
            }

        }


        @Override
        public void onDraw(Canvas canvas) {
            canvas.drawColor(Color.TRANSPARENT);
            paint.setStyle(Paint.Style.FILL);
            for (Point point : points) {
                canvas.drawCircle(point.x, point.y, 10, paint);

                // Log.d(TAG, "Painting: "+point);
            }
            if (points.size() >= 3){
                float x0 = points.get(0).x;
                float y0 = points.get(0).y;
                float vx = points.get(1).x;
                float vy = points.get(1).y;
                float x2 = points.get(2).x;
                float y2 = points.get(2).y;
                canvas.drawLine(x0,y0,vx,vy, paint );
                canvas.drawLine(vx,vy,x2,y2, paint );


                float lenVto1 = FloatMath.sqrt((vx - x0) * (vx - x0) + (vy - y0) * (vy - y0));
                float lenVto2 = FloatMath.sqrt((vx -x2)*(vx-x2) + (vy - y2)*(vy-y2));
                float len1to2 = FloatMath.sqrt((x0 - x2)*(x0 - x2) + (y0 - y2)*(y0 - y2));
                double angle = Math.acos((lenVto1*lenVto1 + lenVto2*lenVto2 - len1to2*len1to2)/(2*lenVto1*lenVto2));
                float radius = Math.min(lenVto1,lenVto2)/2;

                float left = vx - radius;
                float right = vx + radius;
                float top = vy - radius;
                float bottom = vy + radius;

                float lenVto0 = FloatMath.sqrt((vx - right)*(vx - right) + (vy - vy)*(vy - vy));
                float len1to0 = FloatMath.sqrt((x0 - right)*(x0 - right) + (y0 - vy)*(y0 - vy));
                float len2to0 = FloatMath.sqrt((x2 - right)*(x2 - right) + (y2 - vy)*(y2 - vy));
                double origStartAngle = Math.acos((lenVto1*lenVto1 + lenVto0*lenVto0 - len1to0*len1to0)/(2*lenVto1*lenVto0));
                double endAngle = Math.acos((lenVto2*lenVto2 + lenVto0*lenVto0 - len2to0*len2to0)/(2*lenVto2*lenVto0));
                origStartAngle = origStartAngle * 180/Math.PI;
                endAngle = endAngle* 180/Math.PI;
                double degAngle = angle* 180/Math.PI;
                double startAngle = origStartAngle;

                //Log.d("angles", startAngle + ", " + endAngle + ", " + degAngle);
                RectF rect = new RectF(left, top, right, bottom);
                paint.setStyle(Paint.Style.STROKE);
                int scenario = 0;
                if (y0 < vy && y2 < vy) {

                   // Log.d("points","both ys less");

                    if (x0 >= x2) {
                        startAngle = -endAngle;
                    }else{
                        startAngle = -startAngle;
                    }
                }
                if(y0 < vy && y2 >= vy){
                    scenario = 1;
                    //Log.d("points","1 y is less 2 y is more");

                    if(x0 <= vx && x2 <= vx){
                        startAngle = endAngle;
                    }
                    else if(x0 >= vx && x2 >= vx){
                        startAngle = -startAngle;
                    }
                    else{
                        if (180 - startAngle < endAngle){
                            startAngle = endAngle;
                        }else {
                            startAngle = -startAngle;
                        }
                    }


                }
                if (y2 < vy & y0  >= vy) {
                    scenario = 2;
                    //Log.d("points","2 y is less 1 y is more");
                    if(x0 <= vx && x2 <= vx){
                        //startAngle = endAngle;
                    }
                    else if(x0 >= vx && x2 >= vx){
                        startAngle = -endAngle;
                    }
                    else{
                        if (180 - endAngle < startAngle){
                            startAngle = startAngle;
                        }else {
                            startAngle = -endAngle;
                        }
                    }

                }

                if (y0 > vy && y2 > vy) {
                    scenario = 3;
                    //Log.d("points","both ys more");
                    if (x0 < x2) {
                        startAngle = endAngle;
                    }else{
                        startAngle = startAngle;
                    }

                }
                if (isConjugate){
                    degAngle = 360 - degAngle;
                    angle = 2* Math.PI - angle;
                    switch (scenario){
                        case 0:
                            if (x0 >= x2) {
                                startAngle = -origStartAngle;
                            }else{
                                startAngle = -endAngle;
                            }
                            break;
                        case 1:

                            if(x0 <= vx && x2 <= vx){
                                startAngle = -origStartAngle;
                            }
                            else if(x0 >= vx && x2 >= vx){
                                startAngle = endAngle;
                            }
                            else{
                                if (180 - origStartAngle < endAngle){
                                    startAngle = -origStartAngle;
                                }else {
                                    startAngle = endAngle;
                                }
                            }
                            break;
                        case 2:
                            if(x0 <= vx && x2 <= vx){
                                //startAngle = endAngle;
                                startAngle = -endAngle;
                            }
                            else if(x0 >= vx && x2 >= vx){
                                startAngle = origStartAngle;
                            }
                            else{
                                if (180 - endAngle < startAngle){
                                    startAngle = -endAngle;
                                }else {
                                    startAngle = origStartAngle;
                                }
                            }
                            break;
                        case 3:
                            startAngle = startAngle;
                            if (x0 < x2) {
                                startAngle = origStartAngle;
                            }else{
                                startAngle = endAngle;
                            }
                            break;


                    }

                }
                    setDegrees(degAngle);
                    setRadians(angle);
                    canvas.drawArc(rect,(float)startAngle, (float)degAngle,false,paint);


            }
        }
    }

    }

