package com.zootowntech.geometryit;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
//import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
//import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ToolsListFragment extends ListFragment {
    ArrayAdapter<String> adapter;
    boolean mTwoPane;

    public void setDisplayType(boolean isTwoPane){
        mTwoPane = isTwoPane;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        String[] terms = new String[] { "3D Shapes" , "Transformations", "Protractor"};
        ArrayList<String> termsList = new ArrayList<String>();
        termsList.addAll( Arrays.asList(terms) );
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        // Create ArrayAdapter using the planet list.
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.my_list_view,R.id.list_content, termsList);
        setListAdapter(adapter);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_toolslist, container, false);
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String myUrl= null;

        if (position == 0){
            myUrl = "http://www.geometry-it.appspot.com/3d_main";
        }
        else if(position == 1){
            myUrl = "http://www.geometry-it.appspot.com/transformations";
        }

        Fragment newFragment;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (position <= 1){
            newFragment = new DetailFragment();
            ((DetailFragment) newFragment).setUrl(myUrl);
        } else{
            newFragment = new ProtractorFragment();
            //((ProtractorFragment) newFragment)
        }


        if (mTwoPane){
			  /*Intent intent = new Intent(getActivity(),DetailActivity.class);
			  intent.putExtra("url", "http://localhost:8081");
			  startActivity(intent);*/
            transaction.replace(R.id.detail_container, newFragment);

        }
        else{

            transaction.replace(R.id.container, newFragment);

        }

        transaction.addToBackStack("Tools");
        transaction.commit();
    }
}